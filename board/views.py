from django.shortcuts import render, HttpResponse, redirect
from .forms import StatusForm
from .models import Status
def index(request):

	form = StatusForm()
	context = {'form' : form, 'statuses' : Status.objects.all()}
	return render(request, 'board/index.html', context)

def save_post(request):

	# fungsi ini hanya dipanggil ketika ngepos.
	if(request.method=="POST"):
		form = StatusForm(request.POST)
		if(form.is_valid()):

			new_status = Status(text = form.cleaned_data["status_input"])
			new_status.save()

	return redirect(index)