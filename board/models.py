from django.db import models

class Status(models.Model):

	text = models.TextField()
	submitted_time = models.DateTimeField(auto_now_add = True)