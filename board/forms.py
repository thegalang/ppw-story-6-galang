from django import forms

class StatusForm(forms.Form):

	status_input = forms.CharField(widget = forms.Textarea(attrs={"rows":6, "cols":40, 
														"class": "form-control",
														"placeholder": "Ceritakan kabarmu? (maksimum 300 karakter)"}), 
						   max_length=300, label="")